
# markitberry

[![NPM version][npm-image]][npm-url]
[![Build status][travis-image]][travis-url]
[![Test coverage][coveralls-image]][coveralls-url]
[![Dependency Status][david-image]][david-url]
[![License][license-image]][license-url]
[![Downloads][downloads-image]][downloads-url]
[![Gittip][gittip-image]][gittip-url]

My personal markdown parser for both node.js and the client.

Features:

- Emojis
- Autolinking
- Syntax highlighting for common languages
- Shit like &hellip;

## API

```js
var render = require('markitberry')

var html = render('# LOL')
```

[gitter-image]: https://badges.gitter.im/jonathanong/markitberry.png
[gitter-url]: https://gitter.im/jonathanong/markitberry
[npm-image]: https://img.shields.io/npm/v/markitberry.svg?style=flat-square
[npm-url]: https://npmjs.org/package/markitberry
[github-tag]: http://img.shields.io/github/tag/jonathanong/markitberry.svg?style=flat-square
[github-url]: https://github.com/jonathanong/markitberry/tags
[travis-image]: https://img.shields.io/travis/jonathanong/markitberry.svg?style=flat-square
[travis-url]: https://travis-ci.org/jonathanong/markitberry
[coveralls-image]: https://img.shields.io/coveralls/jonathanong/markitberry.svg?style=flat-square
[coveralls-url]: https://coveralls.io/r/jonathanong/markitberry
[david-image]: http://img.shields.io/david/jonathanong/markitberry.svg?style=flat-square
[david-url]: https://david-dm.org/jonathanong/markitberry
[license-image]: http://img.shields.io/npm/l/markitberry.svg?style=flat-square
[license-url]: LICENSE
[downloads-image]: http://img.shields.io/npm/dm/markitberry.svg?style=flat-square
[downloads-url]: https://npmjs.org/package/markitberry
[gittip-image]: https://img.shields.io/gratipay/jonathanong.svg?style=flat-square
[gittip-url]: https://gratipay.com/jonathanong/
