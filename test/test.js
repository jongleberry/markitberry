
var assert = require('assert')

var render = require('..')

it('should highlight code', function () {
  var html = render('```js\nvar a = 1\n```')
  assert(~html.indexOf('language-js'))
})

it('should not render html by default', function () {
  var html = render('<h1>lol</h1>')
  assert(~html.indexOf('&lt;'))
})

it('should render html if .html=true', function () {
  var html = render('<h1>lol</h1>', {
    html: true
  })
  assert(html.trim() === '<p><h1>lol</h1></p>')
})

it('should auto linkify', function () {
  var html = render('https://github.com')
  assert(html.trim() === '<p><a href="https://github.com">https://github.com</a></p>')
})

it('should support emojis', function () {
  var html = render(':raised_hands:')
  assert(!~html.indexOf('raised_hands'))
})

it('should make nice ellipses', function () {
  var html = render('...')
  assert.equal(html.trim(), '<p>…</p>')
})
