'use strict'

module.exports = render

function render(string, options) {
  options = options || {}
  if (options.html) return render.md_html.render(string)
  return render.md.render(string)
}

render.md = require('markdown-it')({
  html: false,
  linkify: true,
  typographer: true,
  highlight: highlight,
})

render.md_html = require('markdown-it')({
  html: true,
  linkify: true,
  typographer: true,
  highlight: highlight,
})

// written out so browserify can do its thing
render.md.use(require('markdown-it-abbr'))
render.md_html.use(require('markdown-it-abbr'))
render.md.use(require('markdown-it-deflist'))
render.md_html.use(require('markdown-it-deflist'))
render.md.use(require('markdown-it-emoji'))
render.md_html.use(require('markdown-it-emoji'))
render.md.use(require('markdown-it-footnote'))
render.md_html.use(require('markdown-it-footnote'))
render.md.use(require('markdown-it-ins'))
render.md_html.use(require('markdown-it-ins'))
render.md.use(require('markdown-it-mark'))
render.md_html.use(require('markdown-it-mark'))
render.md.use(require('markdown-it-sub'))
render.md_html.use(require('markdown-it-sub'))
render.md.use(require('markdown-it-sup'))
render.md_html.use(require('markdown-it-sup'))

// https://github.com/markdown-it/markdown-it#syntax-highlighting
const hljs = require('highlight.js')
/* istanbul ignore next */
function highlight(str, lang) {
  if (lang && hljs.getLanguage(lang)) {
    try {
      return hljs.highlight(lang, str).value
    } catch (_) {}
  }

  try {
    return hljs.highlightAuto(str).value
  } catch (_) {}

  return '' // use external default escaping
}
